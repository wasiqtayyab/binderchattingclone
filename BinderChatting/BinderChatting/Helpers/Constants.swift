//
//  Constants.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 13/07/2021.
//

import Foundation

let userDefaults = UserDefaults.standard

public let kFILEREFERENCE = "gs://bindercha-1c87d.appspot.com"

public let kNUMBEROFMESSAGES = 12

public let kCURRENTUSER = "currentUser"
public let kSTATUS = "status"
public let kFIRSTRUN = "firstRUN"

public let kCHATROOMID = "chatRoomId"
public let kSENDERID = "senderId"

public let kSENT = "Sent"
public let kREAD = "Read"



public let kTEXT = "text"
public let kPHOTO = "photo"
public let kVIDEO = "video"
public let kAUDIO = "audio"
public let kLOCATION = "location"

public let kDATE = "date"
public let kREADDATE = "readdate"


public let kADMINID = "adminId"
public let kMEMBERIDS = "memberIds"
