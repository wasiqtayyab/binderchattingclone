//
//  MapViewViewController.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 29/07/2021.
//

import UIKit
import MapKit
import CoreLocation

class MapViewViewController: UIViewController {

    //MARK:- Vars
    
    var location: CLLocation?
    var mapView: MKMapView!
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTitle()
        configureMapView()
        configureLeftBarButton()
        
    }
    
    //MARK:- Configuration
    
    private func configureMapView(){
        mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        mapView.showsUserLocation = true
        
        if location != nil {
            mapView.setCenter(location!.coordinate, animated: false)
            mapView.addAnnotation(MapAnnotation(title: "User location", coordinate: location!.coordinate))
            
        }
        
        view.addSubview(mapView)
        
    }
    
    private func configureLeftBarButton() {
        
       // self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(self.backButtonPressed))
        
    }
    
    private func configureTitle(){
        self.title = "Map View"
        
    }
    
    //MARK:- Action
    
    @objc func backButtonPressed(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
}
