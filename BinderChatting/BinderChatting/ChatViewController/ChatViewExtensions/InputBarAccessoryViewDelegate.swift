//
//  InputBarAccessoryViewDelegate.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 27/07/2021.
//

import Foundation
import InputBarAccessoryView

extension ChatViewController: InputBarAccessoryViewDelegate {
    
    func inputBar(_ inputBar: InputBarAccessoryView, textViewTextDidChangeTo text: String) {
        
        if text != "" {
          typingIndicatorUpdate()
        }
        updateMicButtonStatus(show: text == "")
       
    }
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        for component in inputBar.inputTextView.components {
            if let text = component as? String {
               
                messageSend(text: text, photo: nil, video: nil, audio: nil, location: nil)
                
            }
        }
        
        messageInputBar.inputTextView.text = ""
        messageInputBar.invalidatePlugins()
    }
    
}

