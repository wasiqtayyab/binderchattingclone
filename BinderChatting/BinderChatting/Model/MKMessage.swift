//
//  MKMessage.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 27/07/2021.
//

import Foundation
import UIKit
import MessageKit
import CoreLocation

class MKMessage: NSObject, MessageType {
    
    var messageId: String
    var kind: MessageKind
    var sentDate: Date
    var incoming: Bool
    var mkSender: MKSender
    var sender: SenderType { return mkSender }
    var senderInitials: String
    
    var photoItem: PhotoMessage?
    var videoItem: VideoMessage?
    var locationItem: LocationMessage?
    var audioItem: AudioMessage?
    
    var status: String
    var readDate: Date
    
    init(message: LocalMessage) {
        
        self.messageId = message.id
        self.mkSender = MKSender(senderId: "arslan_id1", displayName: "arslan")
        self.status = message.status
        self.kind = MessageKind.text(message.message)

        switch message.type {
        case kTEXT :
            self.kind = MessageKind.text(message.message)
        case kPHOTO :
            let photoItem = PhotoMessage(path: message.pictureUrl)
            self.kind = MessageKind.photo(photoItem)
            self.photoItem = photoItem
            
        case kVIDEO:
            
            let videoItem = VideoMessage(url: nil)
            self.kind = MessageKind.video(videoItem)
            self.videoItem = videoItem
            
        case kLOCATION:
            
            let locationItem = LocationMessage(location: CLLocation(latitude: message.latitude, longitude: message.longitude))
            
            self.kind = MessageKind.location(locationItem)
            self.locationItem = locationItem
           
        case kAUDIO:
            
            let audioItem = AudioMessage(duration: 0.0)
            self.kind = MessageKind.audio(audioItem)
            self.audioItem = audioItem
            
        default:
            self.kind = MessageKind.text(message.message)
            print("unknown message type")
        }
        
        self.senderInitials = message.senderinitials
        self.sentDate = message.date
        self.readDate = message.readDate
        
        print("mkSender.senderId",mkSender.senderId)
        print("User.currentId",User.currentId)
        
        self.incoming = User.currentId != mkSender.senderId
        
        print("incoming",incoming)
        
        
    }
    
}

