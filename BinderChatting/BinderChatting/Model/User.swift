//
//  User.swift
//  Whatsapp Clone
//
//  Created by Wasiq Tayyab on 13/07/2021.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

struct User: Codable,Equatable {
    
    var id = ""
    var username = "wasiq"
    var email: String
    var pushId = ""
    var avatarLink = ""
    var status: String
    
    static var currentId: String{
        return "wasiq_id1"
    }
    
    static var currentUser: User?{
        if Auth.auth().currentUser != nil {
            if let dictionary = UserDefaults.standard.data(forKey: kCURRENTUSER){
                print("kCURRENTUSER",kCURRENTUSER)
                
                let decoder = JSONDecoder()
                do{
                    let object = try decoder.decode(User.self, from: dictionary)
                    return object
                }catch{
                    print("Error decoding",error.localizedDescription)
                }
            }
        }
        return nil
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
        lhs.id == rhs.id
    }
    
}

func saveUserLocally(_ user: User){
    
    let encoder = JSONEncoder()
    do{
        let data = try encoder.encode(user)
        UserDefaults.standard.set(data,forKey: kCURRENTUSER)
        print("kCURRENTUSER",kCURRENTUSER)
    }catch {
        
        print("error saving user locally",error.localizedDescription)
    }
    
}


func createDummyUsers(){
    print("create dummy users")
    let names = ["Jean J. Wilson","Theresa R. Ward","Edward W. Morton","Mary D. Smith","Abby G. Craven","Michelle L. Morgan"]
    var imageIndex = 1
    var userIndex = 1
    
    for i in 0..<6{
        let id = UUID().uuidString
        let fileDirectory = "Avatars/" + "_\(id)" + ".jpg"
        FileStorage.uploadImage(UIImage(named: "user\(imageIndex)")!, directory: fileDirectory) { (avatarLink) in
            let user = User(id: id, username: names[i], email: "user\(userIndex)@gmail.com", pushId: "", avatarLink: avatarLink ?? "", status: "No Status")
            userIndex += 1
            FirebaseUserListener.shared.saveUserToFirestore(user)
        }
        imageIndex += 1
        if imageIndex == 6 {
            imageIndex = 1
        }
        
    }
    
}

